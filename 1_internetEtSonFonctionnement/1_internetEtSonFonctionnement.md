---
marp: true
markdown.marp.enableHtml: true
paginate: false
---
<!-- Global style -->
<style>
:root {
    font-size: 1.5em;
    font-family: 'Arial';
    color: #333;
}
section {
    position: relative;
}
audio {
    width: 100%;
    opacity: .2;
    position: absolute;
    bottom: 0;
    left: 0;
}
video {
    width: 100%;
    height: 100%;
}
</style>

![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h1>Cours 1</h1>
    <p><b>Internet et son fonctionnement</b></p>
</div>

---
![bg left:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/lightspeed.webp)

## Internet

Aujourd'hui, je vous propose de découvrir ensemble comment cela fonctionne. Nous allons voir des informations qui peuvent aller à la vitesse de la lumière, le traitement de vos données et comment rester en "sécurité". 

Prêts à plonger dans les coulisses d'Internet ?

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

## Avant de commencer

- **Avez-vous des exemples des usages d'Internet dans votre vie ?**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

## Avant de commencer

- _Avez-vous des exemples des usages d'Internet dans votre vie ?_
- **Pensez-vous qu'il est important de comprendre comment cela fonctionne ?**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

## Avant de commencer

- _Avez-vous des exemples des usages d'Internet dans votre vie ?_
- _Pensez-vous qu'il est important de comprendre comment cela fonctionne ?_
- **Quelqu'un pense-t-il avoir une idée de comment cela fonctionne ?**

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie.webp)

## N'y allons pas seul

Nous n'allons tout de même pas nous aventurer seul dans les méandres d'Internet ! 

Mademoiselle **Sophie**, qui, comme vous pouvez le voir, nous fait coucou, va nous accompagner tout au long de ce cours. Et peut-être même nous servir d'exemple !

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/bonjour.mp3"></audio>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 1 : Les bases d'Internet
    </h2>
</div>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/clock.webp)

### Historique rapide

- Entre 1940 et 1958 : Démonstration du contrôle d'ordinateur à distance et de la transmission de données par téléphone
- De 1962 à 1971 : Mise en place de la théorie d'un réseau de communication mondial et naissance du projet ARPANET
- En 1973 : Création du protocole TCP/IP pour la communication entre ordinateurs
- De 1979 à 1991 : La véritable naissance d'Internet avec la création du World Wide Web
- Puis en 2014 : Explosion d'Internet avec plus d'un milliard de sites Web

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_dubitative.webp)

### Une question de Sophie

Tu veux intervenir Sophie ?

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/quest_histoire.mp3"></audio>

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_nani.webp)

### Une question de Sophie

Non, bien sûr que non. Comme dans toutes sciences, plusieurs groupes de chercheurs ont travaillé sur des projets similaires en même temps. 

Dans les années 60, plusieurs projets de réseaux informatiques ont été lancés, dont un au Japon et un autre en France dans les années 70.

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/minitel.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;top: 10px;width: 20em;">Voici une des versions du Minitel. Il y en a eu plusieurs, ils furent même distribués gratuitement.<br><br>Des ordinateurs gratuits, imaginez !</p>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/map.webp)

### Définition d'Internet

Internet, c'est un réseau mondial, qui est disponible dans tous les pays du monde. Ce sont des milliers d'ordinateurs qui communiquer entre eux via des "paquets" d'informations.

Ces communications peuvent être des messages, des images, des vidéos et bien d'autres choses encore. Pour échanger ces informations, il faut que tous les ordinateurs utilisent la mêmes langue, les mêmes règles, c'est ce qu'on appelle des "protocoles". Et il y en a beaucoup, on pourrait même dire que c'est la guerre des protocoles !

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_dubitative_2.webp)

### Une question de Sophie

Tu as une question Sophie ? Tu fais une drôle de tête !

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/quest_laptop.mp3"></audio>

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_soulagee.webp)

### Une question de Sophie

C'est une bonne remarque et je comprends que cela puisse être un peu compliqué. Mais ne t'inquiète pas, je vais t'expliquer !

Un ordinateur est une machine qui peut traiter des informations, comme un téléphone, une tablette ou un ordinateur portable

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/internet_map_1024.jpg)

### Le nuage Internet

Internet, est un immense réseau de communication, lui même composé de réseaux plus petits. On le compare souvent à une toile d'araignée qui relie des milliards d'ordinateurs entre eux. Mais je préfère le comparer à un univers.

Il y a actuellement plus de 1,977,999,420 sites Internet en ligne. C'est 500 millions de plus que le nombre de personnes qui vivent en Chine, le pays le plus peuplé du monde. Et 500 millions, c'est plus que le nombre de personnes qui vivent aux Etats-Unis, le troisième pays le plus peuplé du monde.

---

![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/internet_map_1024.jpg)

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 2 : Connexion et communication
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Les différents types de connexion

Il existe de nombreux moyens de se connecter à Internet. 

- L'**ADSL** (Asymmetric Digital Subscriber Line) utilise les câbles téléphoniques.
- La **fibre optique**, utilise des câbles en verre ou en plastique.
- La **3/4/5g** utilise des ondes radio.
- ...

Vous en connaissez d'autres ?

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/adsl.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;top: 10px;width: 20em;">
Voici un schéma de branchement d'une connexion ADSL. Les données sont transmises par des fils de cuivre.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/fibre_optique.webp)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;bottom: 10px;width: 20em;">
La composition d'un câble de fibre optique.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/5g.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;bottom: 10px;width: 20em;">
La 5G et ses versions précédentes utilisent des ondes radio pour transmettre les données. Nous utilisons des antennes et ce sont les mêmes que celles utilisées par les téléphones portables.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/binary_signal.png)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;bottom: 10px;width: 20em;">
Un signal binaire est converti en ondes ou en impulsions analogiques pour être transmis par les câbles ou ondes radio.
</p>

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_dubitative_2.webp)

### Une question de Sophie

Toi et tes questions Sophies ! Tu veux intervenir ?

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/quest_cablemarin.mp3"></audio>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/cable_batoto.jpeg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
Voici un câble sous-marin. Il est utilisé pour relier les continents entre eux. Il est grand !
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/requin.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
Même les requins aiment Internet !
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/cable-soumarin.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
Une personne fait une repparation des fibres optiques.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/fonds-marins-infographie.jpg)

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/map_cable_soumarin.jpg)

<p style="  position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;top: 10px;width: 20em;">
La carte des câbles sous-marins. Ils y en a plus de 400 qui relient les continents.
</p>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/box.webp)

### Routeurs et modems

C'est bien beau de parler de connexion, mais un ordinateur parle avec des 0 et des 1, pas avec des ondes et des câbles. Il faut donc des appareils pour convertir ces signaux.

Les modems sont justement là pour faire la traduction, il est aussi directement connecté à votre fournisseur d'accès, que vos parents payent tous les mois pour avoir accès à Internet et avoir une adresse IP.

Il y a aussi les routeurs, qui sont là pour permettre à plusieurs appareils de se connecter à Internet en même temps et de se parler entre eux.

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/56k.mp3"></audio>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/ip.gif)

### Les adresses IP

Si vous envoyez une lettre, vous devez mettre une adresse sur l'enveloppe. Pour téléphoner, vous devez composer un numéro. Sur Internet, c'est pareil, chaque appareil a une adresse unique, c'est ce qu'on appelle une adresse IP.

- **Adresse IP locale :** L'adresse de votre ordinateur à la maison (ex: 192.168.1.12)
- **Adresse IP publique :** L'adresse de votre maison, qui est visible depuis Internet (ex: 10.8.123.76)
- **IPv4 :** L'ancienne version d'adresse IP 0.0.0.0
- **IPv6 :** La nouvelle version d'adresse IP 2001:0db8:85a3:0000:0000:8a2e:0370:7334

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Les DNS

Les adresses IP sont très pratiques pour les ordinateurs, mais pas pour les humains.

**Vous préférez écrire et retenir `https://fr.wikipedia.org` ou `https://2a02:ec80:600:ed1a::1` ?**

- Mais c'est quoi un DNS ?
- Connaissez-vous d'autres nom de domaine ?

---
![bg contain](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/dns.png)

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Les requêtes Internet

- **DNS :** Votre ordinateur demande au serveur DNS l'adresse IP pour savoir où aller.
- **Requête :** Votre ordinateur envoie une requête au serveur web pour demander la page web.
- **Chemin :** La requête suit un chemin à travers les réseaux de serveurs pour atteindre sa destination.
- **Réponse :** Le serveur web envoie la page web demandée à votre ordinateur.
- **Traitement :** Votre ordinateur affiche la page web et va chercher les images, les vidéos et les autres éléments nécessaires pour l'afficher correctement, comme un puzzle. Ce sont aussi des requêtes.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Les frontières d'Internet

Internet est un réseau mondial sans frontières physiques. Cependant, il existe des "frontières" numériques. Ces frontières sont souvent établies par des lois et des règlements spécifiques à chaque pays ou région.

- **Censure et filtres** : Imposer des restrictions sur les sites web que leurs citoyens peuvent visiter. Ils peuvent bloquer ou filtrer le contenus pour des raisons politiques, sociales ou culturelles.
- **Réglementations de sécurité** : Mettent en place des règles pour protéger les données personnelles et la vie privée des internautes. Cela peut inclure des lois sur la conservation des données, le chiffrement, et l'accès gouvernemental aux informations.
- **Restrictions géographiques** : Certains contenus en ligne sont limités à des régions spécifiques. Par exemple, des films sur des services de streaming peuvent ne pas être disponibles dans tous les pays en raison des droits de diffusion.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Trouver un site web

Pour trouver un site web, On utilise un moteur de recherche comme DuckDuckGo, Google, ...

- **Saisie de mots-clés** : Vous tapez des mots-clés ou des questions dans la barre de recherche (par exemple, "meilleures recettes de gâteau au chocolat").
- **Recherche** : Le moteur de recherche parcourt son immense base de données pour trouver les pages web qui correspondent le mieux à vos mots-clés.
- **Résultats** : Le moteur de recherche vous montre une liste de résultats, classés par pertinence. Vous pouvez alors cliquer sur les liens pour visiter les sites web qui vous intéressent.

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 3 : Les Datacenters
    </h2>
</div>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/ds_openbsd.jpg)

### Qu'est-ce qu'un datacenter ?

Un espace rempli de serveurs, de câbles et de systèmes de refroidissement. Il y en a de toutes les tailles, des petits qui tiennent dans une pièce à des immenses qui occupent des bâtiments entiers.

- **Serveurs :** Stockent les données, calculent les informations et les envoient aux utilisateurs.
- **Câbles :** Relient les serveurs entre eux et aux réseaux de communication.
- **Refroidissement :** Empêche les serveurs de surchauffer et de tomber en panne.

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/c14_old.jpg)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
Base militaire de la guerre froide, maintenant abandonnée et transformée.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/C14_new.png)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;bottom: 10px;width: 20em;">
Transformée en datacenter, elle stocke des milliards de données et est destinée à la conservation et l'archivage numériques.
</p>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<p style="height: 100%;width:100%;text-align: center;">
<video controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/Pantin le plus grand Data center de France.mp4"></video>
</p>

---

![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Rôle des datacenters dans le fonctionnement d'Internet

Les datacenters sont essentiels pour le bon fonctionnement d'Internet.

- **Stockage des données :** Les datacenters stockent les données pour les sites web, les services de streaming, les réseaux sociaux, les jeux en ligne, etc. Sans eux, nous ne pourrions pas accéder rapidement à nos vidéos préférées ou à nos documents en ligne.
- **Traitement des données :** Ils ne se contentent pas de stocker les données, ils les traitent aussi. Par exemple, lorsque vous faites une recherche sur Internet, les datacenters analysent et renvoient les résultats en quelques secondes.
- **Sécurité :** Les datacenters sont conçus pour protéger les données contre les cyberattaques, les pannes de courant et les catastrophes naturelles. Ils ont des systèmes de sécurité avancés pour s'assurer que les informations restent sûres et accessibles.

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_pascontente.webp)
### Intervention de Sophie !

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/temoquepas.mp3"></audio>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/google-datacenter-tape-library-wallpaper-preview.jpg)

### Exemple de grands datacenters

Certaines des plus grandes entreprises technologiques possèdent des datacenters gigantesques. Le plus grand "Citadel Data Center de Switch" aux États-Unis, fait plus de 120 terrains de football.

- **IBM :** IBM a des datacenters dans le monde entier, y compris aux États-Unis, en Europe et en Asie. Ils fournissent des services cloud, IA, blockchain et bien d'autres.
- **Google :** Google a des datacenters partout dans le monde, notamment aux États-Unis, en Europe et en Asie. Ces centres gèrent des milliards de recherches chaque jour.
- **Amazon :** Amazon Web Services (AWS) possède de nombreux datacenters qui fournissent des services cloud à des entreprises et des particuliers.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<p style="height: 100%;width:100%;text-align: center;">
<video controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/IBM Cloud Data Center.mp4"></video>
</p>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 4 : Impact environnemental
    </h2>
</div>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/nuc.jpg)

### Consommation d'énergie

Pout leur fonctionnement, et le refroidissement des serveurs, les datacenters consomment énormément d'énergie.

- **Énergie élevée :** Les datacenters consomment plus d'électricité qu'une ville moyenne comme Meulan-en-Yvelines.
- **Impact climatique :** Contribuent aux émissions de gaz à effet de serre.
- **Prévision 2030 :** Consommation potentielle jusqu'à 8% de l'électricité mondiale.

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/evap_cooling.jpg)

### Consommation d'eau

Les datacenters ont également besoin d'eau pour refroidir les serveurs. Cela peut poser des problèmes dans les régions où l'eau est rare.

- **Eau massive :** Utilisation de millions de litres d'eau par jour pour refroidir.
- **Problèmes locaux :** Épuise les ressources dans les régions avec pénuries d'eau.

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/water_cooling.jpg)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;top: 10px;width: 20em;">
Refroidissement d'un serveur par circuit d'eau
</p>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Matières premières

Comme pour toutes nos technologies, les datacenters nécessitent des composants qui ont un impact environnemental. C'est l'une des raisons pour lesquelles il est important de ne pas changer de téléphone tous les ans. Donc Internet pollue.

- **Ressources intensives :** Nécessitent des matériaux comme le cuivre et les terres rares.
- **Extraction destructive :** Déplacement de montagnes, destruction d'habitats naturels, pollution des eaux.
- **Exploitation humaine :** Conditions de travail dangereuses et non éthiques pour extraire les matières premières.

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/eco_gros.jpg)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;bottom: 10px;width: 20em;">
Destruction d'une montagne pour l'extraction de charbon.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/eco_petit.jpg)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
Mine de soufre, et son extraction manuel sans protections.
</p>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/hdd.png)

### Gestion des déchets

Les datacenters produisent également des déchets, comme les serveurs obsolètes et les composants électroniques usagés.

- **Remplacement fréquent :** Les appareils électroniques sont souvent remplacés.
- **Déchets électroniques :** Production de plusieurs tonnes de déchets par an.
- **Matériaux toxiques :** Besoin de recyclage et de traitement pour éviter la pollution.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Moins polluer ?

Il existe des moyens de réduire l'impact environnemental d'Internet par nos choix et nos actions.

- **Réduire les données :** Eviter de regarder des vidéos en 4k de pranks, éviter stockage inutile...
- **Datacenters "verts" :** Utilisation d'énergies renouvelable, optimiser les logiciels...
- **Pouvoir du consommateur :** Boycott des services pollueurs pour influencer les choix entreprises.

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 5 : La Sécurité et Internet
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/https.jpg)

### Qu'est-ce qu'une connexion sécurisée (HTTPS) ?

C'est la fusion de deux protocoles, HTTP et SSL/TLS. Un pour la communication et l'autre pour la sécurité.

- **HTTP :** Hypertext Transfer Protocol, c'est le protocole de communication entre votre ordinateur et le serveur web.
- **SSL/TLS :** Secure Sockets Layer/Transport Layer Security, c'est le protocole de sécurité qui chiffre les données pour les protéger des pirates informatiques.

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/enigma.webp)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 20em;">
La machine Enigma, utilisée par les nazis pour chiffrer leurs communications pendant la Seconde Guerre mondiale.
</p>

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/https-green.png)
### Une connexion sécurisée assure t'elle la sécurité de la personne ?

Non, elle permet uniquement d'être sûr, que personne ne pourra écouter la conversation entre vous et le site. Et ne protège pas contre les sites malveillants. Comme :

- **Le phishing :** Des sites trompeurs qui ressemblent à des sites officiels pour voler vos informations, comme vos mots de passe ou vos numéros de carte bancaire...
- **Typosquatting :** Des sites qui utilisent des noms de domaine similaires à des sites populaires pour vous tromper.
- **Les arnaques :** Quelque soit l'arnaque, elle peut être sur Internet et avoir une connexion sécurisée. Cela reste une arnaque.

---
![bg contain](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/typosquat.png)

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/fake-news.jpg)
### Fake news, désinformation et réseaux sociaux

Les fake news, ou fausses informations, se propagent souvent sur les réseaux sociaux.

- Créées pour tromper, influencer ou pour des raisons financières.
- Importance de vérifier les sources et de se méfier des titres accrocheurs.
- Apprendre à repérer les signes de désinformation.
- L'esprit critique, une approche scientifique et le scepticisme sont essentiels pour ne pas se faire avoir.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Fake news, désinformation et réseaux sociaux

Épinglé par la justice, des entreprises et influenceurs n'hésitent pas à diffuser des informations fausses pour gagner de l'argent.

- **Facebook :** A payé 750 millions de dollars a la justice américaine pour éviter un procès sur son aide au président Trump et la Russie afin de manipuler les élections présidentielles.
- **Mélanie Orl :** Influenceuse, poursuivie par la répression des fraudes pour avoir fait de la publicité mensongère sur des produits illégaux en France et promotion des paris sportifs abusifs.
- **Sophie Fantasy :** Condamnée à 3 ans de prison pour escroquerie en bande organisée, abus de faiblesse et harcèlement moral.
- ... Bien d'autres entreprises et influenceurs sont en attentent de jugement pour des faits similaires.

Alors pourquoi ?

---
![bg contain](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/influence.png)

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/gafams.jpg)
### Contrôle d'Internet et risques

Certaines grandes entreprises, comme Google, exercent un contrôle considérable sur Internet.

- Google domine plusieurs services en ligne (recherche, navigation, visioconférence, courriel).
- Centralisation des informations et implications légales internationales.
- Les GAFAM (Google, Apple, Facebook, Amazon, Microsoft) détiennent un pouvoir immense.
- Organisations comme la Quadrature du Net, Mozilla et la Free Software Foundation défendent nos droits.

---
![bg left:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/darkweb.jpg)
### Deep web et dark web

Le deep web et le dark web sont des parties non indexées de l'Internet, avec des implications et des risques différents.

- Le deep web comprend des pages privées et non indexées par les moteurs de recherche.
- Le dark web utilise des protocoles et anonymise certaines communications pour les protéger.
- Utilisé par les journalistes, opposants politiques et lanceurs d'alerte.
- Également utilisé par des criminels pour des activités illégales (vente d'armes, drogue, etc.).

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 6 : En pratique !
    </h2>
</div>

---
![bg contain](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/nav.png)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;left: 10px;bottom: 10px;width: 30em;">
Choisir un navigateur web. Il y en a 3 principaux, les autres sont généralement des copies modifiées de Chromium qui appartien a Google.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/nav_use.png)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;bottom: 10px;width: 20em;">
Entrer une URL ou faire une recherche.
</p>

---
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/wikipedia.png)

<p style="position: absolute;border-radius: 10px; padding: 10px; background-color: #fff;right: 10px;top: 10px;width: 10em;">
Organisation d'un page web. Header, Content, Footer.
</p>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Petite démonstration !
    </h2>
</div>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Des questions ?
    </h2>
</div>

---

![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_bye.webp)
### Au revoir Sophie !

<audio controls src="https://gitlab.com/adrienK/courscm2/-/raw/main/assets/aurevoir.mp3"></audio>
