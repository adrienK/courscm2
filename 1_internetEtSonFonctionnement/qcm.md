---
marp: true
markdown.marp.enableHtml: true
paginate: false
---
<!-- Global style -->
<style>
:root {
    font-size: 1.8em;
    font-family: 'Arial';
    color: #333;
}
section {
    position: relative;
}
p {
  text-align: center;
  font-weight: bold;
  padding-bottom: 1rem;
}
div.marpit > svg > foreignObject > section li, li {
    color: #1A5FB4 !important;
    border: 2px solid !important;
    padding: .1rem !important;
    font-size: .6rem !important;
    list-style: upper-alpha !important;
    border-radius: 10px !important;
}
</style>

![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h1>Cours 1</h1>
    <p><b>QCM: Internet et son fonctionnement</b></p>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les origines d'Internet

Quel projet est considéré comme le précurseur d'Internet ?

1. **Cytronade**
1. **ARPANET**
1. **Minitel**
1. **Projet Pizza**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les protocoles d'Internet

Que fait le protocole TCP dans le cadre des communications sur Internet ?

1. **Il divise les messages en petits paquets et s'assure qu'ils arrivent sans erreur**
1. **Il adresse et dirige les paquets vers des destinataires aléatoires**
1. **Il permet de créer des liens entre les pages Internet**
1. **Il transforme les messages en ananas pour fournir une connexion plus juteuse**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Le World Wide Web

Qu'est-ce que le World Wide Web (www) ?

1. **Un protocole de communication pour les ordinateurs**
1. **Un réseau militaire top secret**
1. **Un club de fans de toiles d'araignées**
1. **Un système de documents reliés par des liens**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les nœuds et relais Internet

À quoi servent les nœuds et les relais sur Internet ?

1. **Ils stockent toutes les vidéos de chats mignon**
1. **Ils traduisent les messages en différentes langues**
1. **Ils aident les messages à voyager d'un ordinateur à un autre sur le réseau**
1. **Ils fabriquent des ordinateurs portables**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## La taille d'Internet

Combien y a-t-il actuellement de sites Internet en ligne ?

1. **Moins de 500 millions**
1. **Plus de 1,977,999,420**
1. **Exactement 1 milliard**
1. **Autant que de grains de sable sur une plage**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 2 : Connexion et communication
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les différents types de connexion

Quel type de connexion utilise des câbles en verre ou en plastique pour transmettre des données à la vitesse de la lumière ?

1. **ADSL**
1. **Fibre optique**
1. **Wi-Fi**
1. **Câble spaghetti**
---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Routeurs et modems

Quel appareil convertit les signaux numériques de votre ordinateur en signaux analogiques pour utiliser Internet ?

1. **Un Routeur**
1. **Un Modem**
1. **Une Box Internet**
1. **La Machine à café**
---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les adresses IP

Quelle est la principale différence entre une adresse IP locale et une adresse IP publique ?

1. **Tout le monde peut voir votre adresse IP locale, mais pas votre adresse IP publique**
1. **Ce sont les mêmes, mais avec des chiffres différents.**
1. **Les adresses IP locales sont réservées aux chats, tandis que les adresses IP publiques sont pour les chiens.**
1. **L'IP locales est comme notre porte de chambre, les IP publiques notre adresse postale.**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les DNS

Pourquoi utilisons-nous des noms de domaine (DNS) au lieu d'adresses IP pour accéder aux sites web ?

1. **C'est plus pratique et reconnaissable**
1. **Macron n'aime pas les adresses IP**
1. **Il est impossible d'utiliser des adresses IP**
1. **Parce qu'ils ont meilleur goût**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les requêtes Internet

Que se passe-t-il lorsqu'une requête Internet est envoyée ?

1. **Le serveur envoie la page web demandée à votre ordinateur.**
1. **On utilise un pigeon voyageur avec un message au serveur web.**
1. **L'ordinateur va chercher l'adresse puis demander la page web**
1. **Il ne se passe rien, c'est la magie d'Internet.**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Les frontières d'Internet

Qu'est-ce que la "géo-restriction" sur Internet ?

1. **La surveillance des communications en ligne par le gouvernement.**
1. **La protection des données personnelles des internautes.**
1. **La construction de murs en ligne pour séparer les utilisateurs.**
1. **La limitation d'accès à certains contenus en fonction de la région du monde**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Trouver un site web

Quel est le rôle d'un moteur de recherche ?

1. **Trouver des informations sur internet en fonction des mots-clés que vous tapez.**
1. **Stocker les données des utilisateurs.**
1. **Un moteur de voiture nouvelle génération.**
1. **Faire des pizzas sur commande.**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 3 : Les Datacenters
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Qu'est-ce qu'un datacenter ?

Qu'est-ce qu'un datacenter ?

1. **Un bâtiment dédié à la lutte contre les serveurs**
1. **Un centre commercial pour ordinateurs**
1. **Un endroit où les ordinateurs vont en vacances**
1. **Un lieu où sont stockées et gérées des serveurs informatiques**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Rôle des datacenters

Quel est le rôle principal des datacenters dans le fonctionnement d'Internet ?

1. **Ils hébergent des serveurs pour gérer des données et applications importantes**
1. **Ils servent de parking pour les voitures intelligentes**
1. **Ils fournissent des services de stockage et de traitement des données**
1. **Ils protègent contre les cyberattaques et assurent la continuité des services numériques**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Exemple de grands datacenters

Lequel est un grand datacenters Français ?

1. **Un vieux bunker militaire reconverti en datacenter**
1. **La boulangerie du coin**
1. **Une piece secrete dans la tour Eiffel**
1. **La mairie de Meulan-en-Yvelines**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Nombre de données stockées

Quel est le poids estimé d'internet ?

1. **Plus de 2,5 milliards de gigaoctets**
1. **Autant que dans une boîte de céréales**
1. **Moins de 100 gigaoctets**
1. **Exactement 1 million de gigaoctets**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Sécurité des datacenters

Quel type de sécurité peut-on trouver dans un datacenter ?

1. **Détecteurs de mouvement et caméras de surveillance**
1. **Des chiens robots ninja**
1. **Portes blindées et contrôles d’accès biométriques**
1. **Systèmes de sécurité pour résister à une explosion nucléaire**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 4 : Impact environnemental
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Consommation d'énergie

Quel est la consommation énergétique d'un grand datacenter ?

1. **Prés de 3 fours a pizza (j'aime les pizzas)**
1. **Plus que la ville de Meulan-en-Yvelines**
1. **Moins que la ville de Meulan-en-Yvelines**
1. **Probablement la consommation d'un grille-pain géant**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Matières premières

Quels sont les problèmes de fabrication de nos technologies numériques ?

1. **Il n'y en a pas**
1. **Nous devons attendre trop longtemps le prochain iPhone**
1. **La pollution de nos ressources et l'exploitation humaine**
1. **Le manque de chocolat**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Gestion des déchets

Que contiennent les déchets électroniques produits par les datacenters ?

1. **Des matériaux toxiques**
1. **Des matériaux inoffensifs**
1. **Des matériaux comestibles**
1. **Des matériaux magiques**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Moins polluer

Comment peut-on (NOUS) réduire l'impact environnemental d'Internet ?

1. **Utiliser des fournisseurs et des services qui font attention**
1. **Regarder des vidéos en 4K tout le temps**
1. **Utiliser des datacenters qui fonctionnent au charbon**
1. **Acheter un nouveau téléphone chaque semaine pour être à la mode**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 5 : La Sécurité et Internet
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Connexion sécurisée (HTTPS)

Que signifie le "s" dans HTTPS ?

1. **Secure**
1. **Simple**
1. **Speedy**
1. **Supercalifragilisticexpialidocious**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Sécurité de la personne

Nous sommes toujours en sécurité avec une connexion HTTPS ?

1. **Non, elle protège seulement les données pendant leur transfert**
1. **Oui, elle valide votre abonnement à la salle de sport**
1. **Non, elle empêche uniquement les virus**
1. **Oui, et elle permet d'éviter les arnaques en ligne**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Fake news et réseaux sociaux

Pourquoi est-il important de vérifier les sources d'information sur les réseaux sociaux ?

1. **Pour éviter de croire et de partager des fausses informations**
1. **Pour gagner des points bonus sur Facebook**
1. **Pour impressionner ses amis avec des connaissances inutiles**
1. **Pour devenir un détective privé**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Contrôle d'Internet

Qu'est-ce que les GAFAM ?

1. **Google, Apple, Facebook, Amazon, Microsoft**
1. **Geronimo, Alphonse, François, Albert, Marcel**
1. **Gopher, Arpanet, Firefox, Apache, MySQL**
1. **Girafe, Ananas, Fraise, Avocat, Moutarde**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Deep web et dark web

Quelle est la principale différence entre le deep web et le dark web ?

1. **Le deep web est dangereux, le dark web est sûr**
1. **Le deep web est la partie non indexée d'Internet, le dark web utilise des protocoles anonymes**
1. **Le deep web est réservé aux scientifiques, le dark web aux artistes**
1. **Le deep web est fait de cacao, le dark web de vanille**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 6 : En pratique !
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Choisir un navigateur web

Lequel de ces logiciels est un navigateur web ?

1. **LibreOffice**
1. **Firefox**
1. **GIMP**
1. **Minecraft**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Entrer une URL ou faire une recherche

Que signifie "URL" ?

1. **Uniform Resource Locator**
1. **Unité de Recherche en Ligne**
1. **Utilisateur Régulier de Logiciels**
1. **Univers Réel et Lumineux**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Visiter le site ou la web app

Lequel de ces éléments est un logiciel ?

1. **Youtube**
1. **Wikipedia**
1. **La pizza ananas**
2. **Firefox**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
## Question bonus !

Laquelle de ces affirmations est fausse ?

1. **Il y a des fruits dans les jus de fruit**
1. **Pour utiliser Google il faut l'installer**
1. **Internet et le numérique ont un impact sur nos vies**
1. **Tous les ordinateurs peuvent fournir un service sur Internet**
