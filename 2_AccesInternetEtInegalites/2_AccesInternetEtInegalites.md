---
marp: true
markdown.marp.enableHtml: true
paginate: false
---
<!-- Global style -->
<style>
:root {
    font-size: 1.5em;
    font-family: 'Arial';
    color: #333;
}
section {
    position: relative;
}
audio {
    width: 100%;
    opacity: .2;
    position: absolute;
    bottom: 0;
    left: 0;
}
video {
    width: 100%;
    height: 100%;
}
</style>

![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h1>Cours 2</h1>
    <p><b>Les inégalités d'accès à Internet</b></p>
</div>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 1 : L'accès à Internet en France
    </h2>
</div>

---
![bg left:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/iot.png)

### Statistiques et accès à Internet
En France, 93% des ménages sont connectés à Internet.

- **6 837 343 Français vivent avec moins de 1 100 euros par mois**
- **Sur 10 Français :**
  - 9 ont accès à Internet en ville, seulement 4 hors des villes
  - 8 possèdent un smartphone
  - 6 peuvent utiliser la fibre optique en ville, sinon seulement 2
  - 4 ont des objets connectés

---
![bg right:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/france.jpg)

### Les zones couvertes vs non couvertes
Alors que dans les villes tout le monde peut accéder à Internet, les petits villages et les zones isolées comme certaines régions montagneuses ont souvent un accès limité ou inexistant à Internet.

Par exemple, les départements de la Creuse et de la Lozère comptent parmi les moins bien couverts en termes de réseau rapide. 

Alors que les grandes métropoles comme Paris, Lyon, et Marseille bénéficient d'une couverture presque complète en fibre optique et en réseaux mobiles de dernière génération.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<iframe src="https://cartefibre.arcep.fr/index.html?lng=2.6340458063048118&amp;lat=47.26418901501441&amp;zoom=6.5&amp;mode=normal&amp;legende=true&amp;filter=true&amp;trimestre=2023T4" style="border:0;height: 100%;" allowfullscreen=""></iframe>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 2 : Les raisons financières
    </h2>
</div>

---
![bg left:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/argent.png)

### Coût des abonnements internet
Le coût des abonnements Internet peut être un obstacle important pour certaines familles. 

Bien que la France, soit l'un des pays où l'accès à Internet est le moins cher, le prix moyen d'un abonnement est de 30 euros par mois, peut représenter une difficulté financière.

Les études montrent que les familles qui gagnent moins de 1 000 euros par mois, ont rarement accès à Internet à domicile, car trop cher pour leur budget.

---
![bg right:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/aide.jpg)

### Aides et subventions disponibles
Pour aider les foyers à accéder à Internet, le gouvernement propose des aides. Cependant, il faut généralement effectuer une demande par Internet et il peut y avoir avoir des personnes réticentes a demander de l'aide.

- Installation de la fibre optique gratuite
- Réductions sur les abonnements pour les familles à faibles revenus
- Programme "Pass Numérique" pour un accès à Internet et une formation numérique

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 3 : Les zones isolées
    </h2>
</div>

---
![bg left:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/montagne.jpg)

### Difficultés
Il existe de nombreux défis pour fournir un accès Internet. L'installation de câbles, de tours de télécommunication et de relais sont très coûteux, les entreprises hésitent donc à investir dans des zones peu peuplées par peur de perdre de l'argent.

Des terrains accidentés ou éloignés rendent la pose de ces infrastructures encore plus difficile et coûteuse.

---
![bg right:33%](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sat.jpg)

### Solutions possibles (satellites, réseaux communautaires)
Pour pallier ces difficultés, plusieurs solutions innovantes sont disponibles

- **Satellites :** Les solutions satellitaires, comme Starlink, offrent une couverture Internet sur le monde entier. Même chez les pingouins en Antarctique.
- **Réseaux communautaires :** Certaines communautés mettent en place leurs propres réseaux. Ces initiatives locales permettent de fournir un accès Internet à des zones autrement négligées par les grands fournisseurs de services.

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

### Conséquences
L'absence d'accès à Internet peut avoir des conséquences graves sur la vie quotidienne des personnes.

- **Difficulté à trouver un emploi**
- **Isolement social et culturel**
- **Difficulté à accéder à l'information**
- **Impossibilité d'utiliser les services publics**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

## Sources
- [ARCEP - Niveau de vie](https://www.insee.fr/fr/outil-interactif/5367857/tableau/30_RPC/31_RNP)
- [ARCEP - Indicateurs d'accessibilité](https://www.arcep.fr/cartes-et-donnees/nos-publications-chiffrees/indicateurs-accessibilite/derniers-chiffres.html)
- [ARCEP - Baromètre du numérique](https://www.arcep.fr/cartes-et-donnees/nos-publications-chiffrees/barometre-du-numerique/le-barometre-du-numerique-edition-2023.html)
- [INSEE - Estimations de population](https://www.insee.fr/fr/statistiques/2012692)
- [Gouvernement.fr - Les aides pour l'accès à Internet](https://www.economie.gouv.fr/cedef/aides-numerique-particuliers)
- [Starlink - Internet par satellite](https://www.starlink.com/)
- [Fédération FDN - Réseaux communautaires](https://www.ffdn.org/)