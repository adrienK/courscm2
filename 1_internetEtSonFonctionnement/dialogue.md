## Chapitre 1 : Internet

Aujourd'hui, je vous propose de découvrir ensemble comment cela fonctionne. Nous allons voir des informations qui peuvent aller à la vitesse de la lumière, le traitement de vos données et comment rester en "sécurité". 

Prêts à plonger dans les coulisses d'internet ?

Le saviez-vous ? Internet vient de l'anglais "interconnected network" qui signifie "réseau interconnecté". C'est un mot invariable, d'après l'Académie Française il n'a pas de pluriel et ne prend pas de majuscule, alors que l'académie du Québec utilise une majuscule, le considérant, à juste titre, comme un nom propre.

### Historique rapide

- Entre 1940 et 1958 : Démonstration du contrôle d'ordinateur à distance et de la transmission de données par téléphone
- De 1962 à 1971 : Mise en place de la théorie d'un réseau de communication mondial et naissance du projet ARPANET
- En 1973 : Création du protocole TCP/IP pour la communication entre ordinateurs
- De 1979 à 1991 : La véritable naissance d'Internet avec la création du World Wide Web
- Puis en 2014 : Explosion d'Internet avec plus d'un milliard de sites Web

Arpanet était le tout premier réseau d'ordinateurs qui a permis à des machines de communiquer entre elles, et la DARPA est l'agence du gouvernement américain qui l'a créé pour aider à développer des technologies avancées. C'était à l'époque de la guerre froide, et les américains voulaient pouvoir communiquer en cas de guerre nucléaire avec l'URSS, l'origine d'Internet est donc militaire.

TCP/IP est un ensemble de règles que les ordinateurs suivent pour échanger des données sur Internet. TCP divise les messages en petits morceaux appelés paquets et s'assure qu'ils arrivent sans erreur, tandis que IP adresse et dirige ces paquets vers le bon destinataire.

Le World Wide Web (www) est un système de documents reliés par des liens hypertextes, que l'on appelle tout simplement des liens, ils prennent la forme de texte ou d'images cliquables. Ces liens nous permettent de passer d'une page à une autre en cliquant dessus. C'est ce qui rend Internet si facile à utiliser.

---
![bg right:33% cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/sophie_nani.webp)

### Une question de Sophie

Tu veux intervenir Sophie ?

#### Réponse
Non, bien sûr que non. Comme dans toutes sciences, plusieurs groupes de chercheurs ont travaillé sur des projets similaires en même temps. 

Dans les années 60/70, plusieurs projets de réseaux informatiques ont été lancés, dont un en France dans les années 70 le projet Cyclades.

Vous avez peut être déjà entendu parler du Minitel ? C'était un terminal informatique qui permettait de consulter des services en ligne, comme des annuaires, des informations pratiques ou des services bancaires. C'était un peu l'ancêtre de l'Internet public.

### Définition d'Internet

Internet, c'est un réseau mondial, qui est disponible dans tous les pays du monde. Ce sont des milliers d'ordinateurs qui communiquer entre eux via des "paquets" d'informations.

Ces communications peuvent être des messages, des images, des vidéos et bien d'autres choses encore. Pour échanger ces informations, il faut que tous les ordinateurs utilisent la mêmes langue, les mêmes règles, c'est ce qu'on appelle des "protocoles". Et il y en a beaucoup, on pourrait même dire que c'est la guerre des protocoles !

### Une question de Sophie

Tu as une question Sophie ? Tu fais une drôle de tête !

#### Réponse

Effectivement, c'est une bonne remarque et je comprends que cela puisse être un peu compliqué. Mais ne t'inquiète pas, tu vas tout comprendre !

Nous n'avons pas défini ce qu'est un "ordinateur". Sache qu'un ordinateur est une machine qui peut traiter des informations, comme un téléphone, une tablette ou un ordinateur portable, exactement comme celui de ta maman.

Et même si Internet ne se compose pas d'ordinateurs portables, tu peux tout à fait le transformer en une brique d'Internet que l'on appelle un "serveur". Retiens bien ce mot, il reviendra !

### Définition d'Internet (suite)

Bon Sophie, laisse moi continuer ! 

Internet, c'est comme un petit univers, chaque planète serait une maison ou un immeuble pouvant contenir plusieurs ordinateurs, chaque soleil un "relai", un système solaire un "nœud", qui lui-même est relié à d'autres "nœuds" au cœur de la galaxie formant un univers de données... Pour te donner une idée, il y a eu plus de 5,16 milliards d'utilisateurs depuis la "création" d'Internet. Ce qui représente plus de la moitié de la population mondiale actuelle.

Pour comprendre un peu mieux, les nœuds et les relais internet sont comme des points de passage qui aident les messages à voyager d'un ordinateur à un autre sur le réseau.

Il y a actuellement plus de 1,977,999,420 sites Internet en ligne. C'est 500 millions de plus que le nombre de personnes qui vivent en Chine, le pays le plus peuplé du monde. Et 500 millions, c'est plus que le nombre de personnes qui vivent aux États-Unis, le troisième pays le plus peuplé du monde.

Je vous laisse admirer une partie de la toile de l'Internet mondial.

## Chapitre 2 : Connexion et communication

### Les différents types de connexion

De nos jours, il existe de nombreux moyens de se connecter à Internet. Les plus courants sont l'ADSL, la fibre optique et le Wi-Fi.

Pour rappel: Seul le binaire est compris par les ordinateurs, c'est à dire des 0 et des 1. Les ordinateurs utilisent des signaux électriques pour transmettre ces informations.

L'ADSL (Asymmetric Digital Subscriber Line = Ligne d'abonné numérique asymétrique) utilise les câbles téléphoniques. Ainsi les données sont transmises par des fils de cuivre, en alternant des vibrations électriques.

La fibre optique, utilise des câbles en verre ou en plastique, ce câble est aussi fin qu'un cheveu humain. Pour la protéger on la recouvre de plusieurs couches de protection, il y a même du kevlar que l'on utilise pour fabriquer des gilets pare-balles. Les données sont transmises par des impulsions de laser, les informations sont donc transmises à la vitesse de la lumière !

La 5g, elle, utilise des ondes radio pour transmettre les données. C'est comme une radio, mais pour les ordinateurs. C'est très pratique car cela permet de se connecter à Internet sans avoir besoin de câbles. A la place de faire vibrer de l'électricité, on fait vibrer des ondes. Très pratique pour les téléphones portables, mais aussi pour les objets connectés.

Vous en connaissez d'autres ?

### Question de Sophie

Toi et tes questions Sophie ! Tu veux intervenir ?

#### Réponse

La aussi il y a des câbles, mais ce sont des câbles sous-marins. Ils sont posés au fond des océans pour relier les continents entre eux. C'est ce qui permet à internet de fonctionner à l'échelle mondiale. Nous pourrions utiliser des satellites, mais cela serait beaucoup plus lent et beaucoup plus cher.

Regarde la taille de ces câbles, ils sont énormes ! Ils peuvent mesurer jusqu'à 10 cm de diamètre et peser plusieurs tonnes. Ils sont conçus pour résister à la pression de l'eau et aux attaques de requins.

Voici la carte des câbles sous-marins qui relient les continents entre eux. Les premiers câbles ont été posés en 1858 pour la communication télégraphique, et le premier câble téléphonique en 1956. Aujourd'hui, ces câbles utilisent de la fibre optique pour transmettre les données, toujours à la vitesse de la lumière.

D'ailleurs, quand une guerre éclate, les câbles sous-marins sont souvent espionnés ou coupés pour empêcher l'ennemi de communiquer. C'est ce qui s'est passé pendant la première et la deuxième guerre mondiale. Ils sont aussi souvent endommagés par des catastrophes naturelles, comme des tremblements de terre ou des tempêtes.

### Routeurs et modems

Un modem convertit les signaux numériques (0 et 1) de votre ordinateur en signaux analogiques (des ondes) pour transmission via des lignes téléphoniques, câbles ou fibres optiques, et vice versa. Il est essentiel pour connecter votre réseau domestique à Internet, que ce soit par ADSL, fibre optique (même si la lumière n'est pas vraiment une onde) ou 5G.

Un routeur connecte différents réseaux et dirige le trafic de données. À la maison, il relie votre réseau local (l'ensemble de vos appareils connectés) à Internet et gère leurs adresses IP locales. Les routeurs modernes incluent souvent le Wi-Fi pour connecter sans fil vos appareils au réseau domestique.

Généralement, le modem et le routeur sont combinés en un seul appareil, appelé "box internet" ou "routeur 5g" par abus de langage. Cela permet d'avoir une seule boîte pour gérer la connexion à Internet et le réseau local sur lequel sont connectés plusieurs appareils.

### Les adresses IP

Si vous voulez envoyez une lettre, vous devez mettre une adresse sur l'enveloppe afin que le facteur sache où la livrer. Pour téléphoner, vous devez composer un numéro de téléphone valide pour que votre appel soit acheminé vers la bonne personne. Sur Internet, c'est pareil, chaque appareil a une adresse pour pouvoir communiquer avec les autres. On appelle cela une adresse IP (Internet Protocol).

Il en existe deux types : les adresses IP locales et les adresses IP publiques.
- Les adresses IP locales sont attribuées aux appareils qui sont connectés sur le même réseau local, comme votre ordinateur, votre téléphone ou votre imprimante. Tout cela au même endroit, chez vous. Elle ressemblent bien souvent à 192.168.1.[1-255]. Si l'on compare cela à  une maison, c'est comme si chaque appareil avait un numéro de chambre.
- Les adresses IP publiques sont attribuées par votre fournisseur d'accès à Internet (FAI) et permettent à votre "maison" de communiquer avec Internet. C'est votre adresse sur Internet, comme l'adresse de votre maison.

Il y a deux formats d'adresses IP, les IPv4 et les IPv6. Nous utilisons les IPv4 (4 294 967 296 ip) depuis les années 80, mais avec l'explosion d'Internet, nous avons commencé à manquer d'adresses IP disponibles. C'est pourquoi nous avons créé les IPv6, qui sont plus longues et permettent de créer plus d'adresses. Comme si nous avions fait des travaux pour agrandir la ville.
- IPv4 : 0.0.0.0 à 255:255:255:255
- IPv6 : 2001:0db8:85a3:0000:0000:8a2e:0370:7334 ... (2^128 ip)

### Les DNS

Bon, vous en conviendriez, pour aller sur un site internet, devoir écrire quelque chose comme "https://2a03:b00:2100:0:23::2", c'est un peu compliqué et pas très pratique. 

Pour simplifier les choses, nous utilisons un système de noms (DNS = Domain Name System). Comme si nous pouvions envoyer une lettre par la poste en écrivant juste le nom du destinataire, sans avoir besoin de son adresse. ces nom sont ensuite traduits en adresses IP par des serveurs DNS et permettent de trouver les sites Web que vous recherchez.

Mais ses noms ne sont pas gratuits, il faut les louer auprès d'un organisme qui gère les noms de domaine. Cela peut aller de quelques euros par an à plusieurs milliers d'euros pour un nom de domaine très recherché.

### Les requêtes internet

Quand vous voulez accéder à une page web, votre ordinateur doit demander cette page à un serveur. Cette demande est appelée une "requête". Imaginez que vous entrez dans une bibliothèque et que vous demandez un livre spécifique à la bibliothécaire. Votre demande est la requête, et le livre que la bibliothécaire vous donne est la réponse.

Le cheminement de votre demande est le suivant :
- **DNS :** Votre ordinateur demande au serveur DNS l'adresse IP pour savoir où aller.
- **Requête :** Votre ordinateur envoie une requête au serveur web pour demander la page web.
- **Chemin :** La requête suit un chemin à travers les réseaux de serveurs pour atteindre sa destination.
- **Réponse :** Le serveur web envoie la page web demandée à votre ordinateur.
- **Traitement :** Votre ordinateur affiche la page web et va chercher les images, les vidéos et les autres éléments nécessaires pour l'afficher correctement, comme un puzzle. Ce sont aussi des requêtes.

### Les frontières d'Internet

Internet est un réseau mondial et sans frontières physiques. A l'origine, il a été conçu pour permettre aux gens de communiquer et de partager des informations à travers le monde, sans restriction, limitation ou censure. Ce principe de liberté et d'ouverture est au cœur de la philosophie d'Internet, il est souvent appelé "Net Neutrality". et est défendu par de nombreuses organisations comme la Free Software Foundation ou en France la Quadrature du Net.

Cependant, les états ont eu du mal à l'accepter, et ont commencé à mettre en place des restrictions pour contrôler l'accès à Internet. C'est ce qu'on appelle la "censure d'Internet". Par exemple, en Chine, le gouvernement bloque l'accès à de nombreux sites web étrangers et surveille les communications en ligne. En Russie, le gouvernement a adopté une loi qui oblige les fournisseurs de services Internet à stocker les données de leurs utilisateurs pendant une certaine période. En France, la loi Hadopi permet une surveillance globale des communications en ligne, dans le but de punir la personne bénéficiant de l'accès à internet en cas de téléchargement illégal par exemple.

Les entreprises, elles aussi, ont commencé à mettre en place des restrictions, ne désirant pas la diffusion d'un contenu dans un pays au profit d'un autre qui pourrait rapporter plus. Par exemple, quand vous allez sur Netflix, vous ne pouvez pas regarder les mêmes films et séries partout dans le monde. C'est ce qu'on appelle la "géo-restriction". C'est une autre forme de frontière numérique.

- **Censure et filtres :** Certains pays imposent des restrictions sur les sites web que leurs citoyens peuvent visiter. Ils peuvent bloquer ou filtrer certains contenus pour des raisons politiques, sociales ou culturelles.
- **Réglementations de sécurité :** Les gouvernements mettent en place des règles pour protéger les données personnelles et la vie privée des internautes. Cela peut inclure des lois sur la conservation des données, le cryptage, et l'accès gouvernemental aux informations.
- **Restrictions géographiques :** Certains contenus en ligne sont limités à des régions spécifiques. Par exemple, des vidéos sur YouTube ou des films sur des services de streaming peuvent ne pas être disponibles dans tous les pays en raison des droits de diffusion.

### Trouver un site web

Pour trouver un site web, on utilise des moteurs de recherche comme DuckDuckGo, Bing et Google. Voici comment cela fonctionne :

1. **Saisie de mots-clés :** Vous tapez des mots-clés ou des questions dans la barre de recherche (par exemple, "meilleures recettes de gâteau au chocolat").
2. **Recherche :** Le moteur de recherche parcourt son immense base de données pour trouver les pages web qui correspondent le mieux à vos mots-clés.
3. **Résultats :** Le moteur de recherche vous montre une liste de résultats, classés par pertinence. Vous pouvez alors cliquer sur les liens pour visiter les sites web qui vous intéressent.

Les moteurs de recherche utilisent des algorithmes complexes pour déterminer quels sites web sont les plus pertinents pour vos mots-clés. Ils prennent en compte de nombreux facteurs, comme la popularité du site, la qualité du contenu, et la pertinence par rapport à votre recherche.

Le premier moteur de recherche a été créé en 1990 par Alan Emtage, un étudiant en informatique à l'Université McGill au Canada. Il s'appelait "Archie" et permettait de trouver des fichiers sur les serveurs FTP. Depuis lors, de nombreux moteurs de recherche ont été développés, mais Google est devenu le plus populaire et le plus utilisé dans le monde entier.

## Chapitre 3 : Les Datacenters

### Qu'est-ce qu'un datacenter ?

Un datacenter, ou centre de données, est un bâtiment dédiée à l'hébergement de serveurs. Ces serveurs sont des ordinateurs puissants qui stockent et gèrent d'énormes quantités de données. Imaginez un datacenter comme une bibliothèque où chaque livre est un serveur et chaque page du livre une page web, une vidéo, une application ou toute autre information accessible sur Internet.

Il y en a de toutes les tailles, des petits datacenters qui tiennent dans une seule pièce, aux grands datacenters qui occupent des bâtiments entiers. Les plus grands datacenters peuvent contenir des milliers de serveurs et consommer autant d'électricité qu'une petite ville.

En France, nous avons un datacenter qui ce nomme C14 de Scaleway, il est situé en région parisienne dans un ancien site militaire construit pendant la guerre froide, un vieux bunker. Il est destiné à stocker des données sensibles, comme des archives, des documents administratifs ou des données de santé. Il a des systèmes de sécurité comme des détecteurs de mouvement, des caméras de surveillance, des portes blindées et des contrôles d’accès biométriques, etc... Il peut même résister à une explosion nucléaire !

### Rôle des datacenters dans le fonctionnement d'Internet

Les datacenters sont important pour le bon fonctionnement d'Internet. Bien que l'on puisse s'en passer pour des petites applications, ils sont indispensables pour les grosses applications comme les moteurs de recherche, les réseaux sociaux, les services de streaming vidéo, les jeux en ligne, etc...

Sans mentionner la sécurité, la protection contre les cyberattaques et des pertes de données. Ils fournissent des centaines de milliers de serveurs pour répondre à nos divers besoins numérique et s'assurer de leur bon fonctionnement. 

Le nombres de données stockées sur ces ordinateurs est supérieur a plus de 2,5 milliards de gigaoctets, cela englobe la totalité des connaissances de l'humanité, de toutes les écritures connues et ce jusqu'à nos jours. Nous avons littéralement une copie complète de notre société au format numérique.

L'ordinateur de la maman de Sophie ne pourrait ni stocker ni traiter toutes les données nécessaires pour faire fonctionner ces applications.

### Exemple de grands datacenters

Certaines des plus grandes entreprises technologiques possèdent des datacenters gigantesques :

- **Google :** Google a des datacenters partout dans le monde, notamment aux États-Unis, en Europe et en Asie. Ces centres gèrent des milliards de recherches chaque jour.
- **Amazon :** Amazon Web Services (AWS) possède de nombreux datacenters qui fournissent des services cloud à des entreprises et des particuliers.
- **Facebook :** Les datacenters de Facebook stockent et gèrent des milliards de photos, vidéos et messages partagés par les utilisateurs.

Imaginez ces datacenters comme de grandes villes remplies de serveurs, tous travaillant ensemble pour s'assurer que vous pouvez regarder une vidéo YouTube sans interruption, faire des achats en ligne, ou envoyer un message instantanément à un ami.

## Chapitre 4 : Impact environnemental

### Consommation d'énergie

Les datacenters consomment une quantité énorme d'énergie. Un grand datacenter peut utiliser plus d'électricité qu'une ville de taille moyenne comme Meulan-en-Yvelines, qui compte environ 9 000 habitants. La consommation énergétique de ces installations contribue aux émissions de gaz à effet de serre, aggravant le changement climatique. Une étude estime que d'ici 2030, les datacenters pourraient consommer jusqu'à 8% de l'électricité mondiale.

### Consommation d'eau

Les datacenters utilisent aussi des quantités massives d'eau pour refroidir leurs serveurs. Par exemple, un datacenter typique peut consommer des millions de litres d'eau par jour. Cette utilisation intensive peut épuiser les ressources locales et causer des problèmes dans les régions souffrant déjà de pénuries d'eau. En île de France, nous avons des restrictions d'eau chaque été, et les datacenters n'aident pas.

### Matières premières

Les terres rares sont des éléments spéciaux trouvés dans le sol. Ils sont utilisés pour fabriquer des objets high-tech comme les téléphones, les ordinateurs et les voitures électriques. Même si on les appelle "rares", ils ne sont pas vraiment si rares, mais ils sont difficiles à extraire et à purifier.

La construction et le fonctionnement des datacenters nécessitent des matériaux comme le cuivre et les terres rares, extraits souvent au prix de dégâts environnementaux considérables. Dans des conditions de travail souvent déplorables, pouvant impliquer des enfants de votre age.

Pour extraire suffisamment de cuivre pour un seul datacenter, il peut être nécessaire de déplacer des montages, détruisant ainsi des habitats naturels et polluant les eaux locales qui vont ensuite se retrouver dans les eaux potables et empoisonner des populations.

Cette pollution est la même que pour produire nos téléphones, nos ordinateurs, nos voitures électriques... Il est important de réfléchir à nos choix d'achats et de ne pas les renouveler trop souvent. En agissant ainsi, nous contribuons à préserver notre planète et à nous éviter des conséquences néfastes, car nous partageons tous la même Terre.

Avec le soufre, nous alons produire de l'acide sulfurique, qui est indispensable pour fabriquer notre electronique. Les vapeurs de soufrent sont toxiques et causent des problemes repiratoires mortels, il peut aussi provoquer de graves brulures voir devenir explosif. Generalement, son extration est realiser par des populations pauvres.

### Gestion des déchets

Les datacenters produisent beaucoup de déchets électroniques lorsque les équipements deviennent obsolètes. 

Un datacenter peut générer plusieurs tonnes de déchets électroniques chaque année. Ces déchets contiennent des matériaux toxiques. Il faut les recycler, les traiter et les éliminer correctement pour éviter de polluer NOTRE environnement et de nuire à notre santé.

### Moins polluer ?

Comment réduire l'impact environnemental a notre échelle ?

- **Réduction des données inutiles :** Trier ses emails, éviter le stockage de photos inutiles, ne pas regarder des vidéos en 4K si ce n'est pas nécessaire. Par exemple, ne pas garder des milliers de photos floues sur votre téléphone.
- **Utilisation de datacenters verts :** Certains datacenters utilisent des énergies renouvelables comme l'énergie solaire ou éolienne pour réduire leur empreinte carbone.
- **Abandonner les pollueurs :** Ne pas utiliser des services qui ne respectent pas l'environnement. 

Une entreprise a pour seul but, l'argent. Si vous n'utilisez pas leurs produits, ils vont vouloir savoir pourquoi et ainsi changer leur façon de faire. C'est ce qu'on appelle le "pouvoir du consommateur". Si vous n'achetez pas, ils ne vendront pas.

## Chapitre 5 : La Sécurité et Internet

### Qu'est-ce qu'une connexion sécurisée (HTTPS) ?

- **http** : HyperText Transfer Protocol, un protocole de communication utilisé pour transférer des données sur Internet.
- **s** : Secure, signifie que la connexion est sécurisée avec un cryptage SSL/TLS.

Une connexion sécurisée, HTTPS (HyperText Transfer Protocol Secure), c'est une manière de sécuriser les communications entre votre navigateur web et le site internet que vous visitez. Imaginez que vous envoyez une lettre dans une enveloppe impossible à ouvrir par quelqu'un d'autre que le destinataire. De cette façon, vos informations sont protégées contre les espions ou les pirates informatiques.

Les sites web sécurisés affichent un cadenas vert dans la barre d'adresse de votre navigateur, ce qui signifie que la connexion est sécurisée. Cela est particulièrement important lorsque vous saisissez des informations sensibles, comme des mots de passe, des numéros de carte de crédit ou des informations personnelles.

SSL c'est un peut comme la machine Enigma de l'armée nazie, elle permet de chiffrer les messages pour qu'ils ne soient pas lus. Nous avons du trouver un moyen de casser le code afin de gagner la guerre. Mais SSL est beaucoup plus compliqué et beaucoup plus sécurisé, actuellement, il faudrait des millions d'années pour casser un code SSL.

### Une connexion sécurisée assure-t-elle la sécurité de la personne ?

Une connexion sécurisée (HTTPS) protège les données pendant leur transfert, mais elle ne dit pas que le site est sûr. Simplement que personne ne pourra écouter la conversation entre vous et le site. 

Vous pouvez toujours être victime de phishing (escroquerie par email) ou télécharger involontairement des logiciels malveillants. C’est comme porter une ceinture de sécurité en voiture : elle réduit les risques de blessures, mais ne vous protège pas contre tous les dangers de la route.

### Fake news, désinformation et réseaux sociaux

Les fake news, ou fausses informations, se propagent souvent sur les réseaux sociaux. 

Elles peuvent être créées pour tromper les gens, influencer leurs opinions ou même pour des raisons financières. 

Il est crucial de vérifier les sources d'information et de se méfier des titres qui donnent envie de cliquer. Apprendre à repérer les signes de désinformation, la seul solution pour ne pas se faire avoir étant l'esprit critique, une approche scientifique et une bonne dose de scepticisme.

En 2016, des fausses informations ont été diffusées sur les réseaux sociaux pour influencer les élections présidentielles américaines. Cela a conduit à des enquêtes sur l'implication de la Russie dans la manipulation des américains afin de manipuler leur vote et donc les résultats des élections. Facebook a notamment participé à la diffusion de ces fausses informations, et dernièrement, ils ont accepté de payer 775 millions de dollars éviter un procès et probablement de la prison pour les dirigeants.

### Contrôle d'internet et risques

Pour faire une recherche sur Internet, vous utilisez probablement Google. Pour naviguer sur le web, vous utilisez probablement Chrome, Opera, Brave, Edge... Qui sont uniquement des variation de Chromium, le navigateur de Google. Pour faire une visio vous utilisez peut être Google Meet, pour vos mails Gmail, Google Docs, Android... 

Google contrôle internet, si ils décident un changement dans la technologie, n'en aime pas une autre, ils peuvent le faire et l'imposer a tous. C'est ce qu'on appelle le contrôle d'internet.

Certaines grandes entreprises, ont cette puissance en raison de leur domination. Cela pose des questions sur le contrôle et la centralisation des informations. Ainsi que vos droits, Google est une entreprise Américaine, et doit se plier aux lois américaines, même si vous êtes en France. En utilisant ces services vous devez donc respecter les lois de cette autre pays, même si elles sont contraires à la loi française.

Ces entreprise ce nomment les GAFAM, Google, Apple, Facebook, Amazon et Microsoft. Plus vous les utilisez, plus vous leur donnez de pouvoir et moins vous pourrez vous en passer. Leur but est de gagner de l'argent, et pour cela, elles sont prêtes à tout pour vous garder.

Des groupements existent pour défendre nos droits, comme la Quadrature du Net en France, Mozilla, ou la Free Software Foundation dans le monde. Ils luttent pour la neutralité du net, la protection de la vie privée et la liberté d'expression.

### Deep web et dark web

Premièrement, je vous conseille de ne jamais aller sur le dark web. Il est facile de se perdre et de tomber sur des choses que vous ne voudriez pas voir et vous marquer à vie.

Le deep web, c'est uniquement la partie d'Internet qui n'est pas indexée par les moteurs de recherche comme Google, comme les pages privées, l'administration d'un site... Si vous n'avez pas le lien, vous ne pouvez pas y accéder. C'est un peut comme refuser d'être dans l'annuaire.

Le dark web, c'est plus compliqué ! Vous vous souvenez ? Nous avons parlé des protocoles, comme le HTTP. Il en existe un autre, et certains furent inventer pour rester complètement anonyme. A l'origine, cette technologie est utilisée pour protéger les journalistes, les opposants politiques, les lanceurs d'alerte... Mais bien sûr, elle est aussi utilisée par les criminels, les pédophiles, les terroristes... On peut y trouver des demandes d'aides dans les pays en guerre, mais aussi de la vente d'armes, de personnes, tueurs à gages et autres horreurs.

## Questions et réponses
A vos questions !
