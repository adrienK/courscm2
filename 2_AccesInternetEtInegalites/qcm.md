---
marp: true
markdown.marp.enableHtml: true
paginate: false
---
<!-- Global style -->
<style>
:root {
    font-size: 1.8em;
    font-family: 'Arial';
    color: #333;
}
section {
    position: relative;
}
p {
  text-align: center;
  font-weight: bold;
  padding-bottom: 1rem;
}
div.marpit > svg > foreignObject > section li, li {
    color: #1A5FB4 !important;
    border: 2px solid !important;
    padding: .1rem !important;
    font-size: .6rem !important;
    list-style: upper-alpha !important;
    border-radius: 10px !important;
}
</style>

![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h1>Cours 1</h1>
    <p><b>QCM: Internet et son fonctionnement</b></p>
</div>

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 1 : L'accès à Internet en France
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Statistiques et accès à Internet

Quel pourcentage des ménages en France sont connectés à Internet ?

1. **70%**
2. **80%**
3. **93%**
4. **100%**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Statistiques et accès à Internet

Combien de Français vivent avec moins de 1 100 euros par mois ?

1. **1 million**
2. **3 millions**
3. **6,837 millions**
4. **10 millions**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Statistiques et accès à Internet

Parmi ces affirmations, laquelle est vraie concernant l'accès à Internet en ville vs hors des villes ?

1. **En ville, 4 Français sur 10 ont accès à Internet**
2. **Hors des villes, 9 Français sur 10 ont accès à Internet**
3. **En ville, 9 Français sur 10 ont accès à Internet**
4. **Hors des villes, 6 Français sur 10 ont accès à Internet**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Les zones couvertes vs non couvertes

Quelles zones en France ont souvent un accès limité ou inexistant à Internet ?

1. **Les grandes métropoles**
2. **Les petits villages et les zones isolées**
3. **Les zones industrielles**
4. **Les zones côtières**
   
---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Les zones couvertes vs non couvertes

Quels départements sont parmi les moins bien couverts en termes de réseau rapide ?

1. **Paris et Lyon**
2. **La Creuse et la Lozère**
3. **Marseille et Bordeaux**
4. **Nice et Lille**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 2 : Les raisons financières
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Coût des abonnements internet

Quel est le prix moyen d'un abonnement Internet en France ?

1. **10 euros par mois**
2. **20 euros par mois**
3. **30 euros par mois**
4. **40 euros par mois**
   
---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
Pourquoi certaines familles en France n'ont-elles pas accès à Internet à domicile ?

1. **Parce qu'elles n'en veulent pas**
2. **Parce qu'elles ne connaissent pas Internet**
3. **Parce que c'est trop cher pour leur budget**
4. **Parce qu'il n'y a pas de fournisseurs Internet**

---
<!-- _class: lead -->
![bg cover](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)

<div style="display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;height: 100%;background-color: #ffffffbf;border-radius: 25px;">
    <h2>
      Chapitre 3 : Les zones isolées
    </h2>
</div>

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Difficultés

Pourquoi les entreprises hésitent-elles à investir dans des zones peu peuplées ?

1. **Parce que les gens n'y veulent pas d'Internet**
2. **Parce que c'est trop coûteux et peu rentable**
3. **Parce qu'il y a trop de concurrents**
4. **Parce que les terrains y sont trop plats comme les œufs**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Difficultés

Quel type de terrain rend l'installation d'infrastructures Internet plus difficile et coûteuse ?

1. **Les terrains plats**
2. **Les terrains accidentés ou éloignés (montagne)**
3. **Les zones industrielles**
4. **Les plages**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Solutions possibles

Quelle solution est mentionnée pour offrir une couverture Internet dans les zones difficiles ?

1. **Utiliser des câbles sous-marins**
2. **Utiliser des pigeons voyageurs**
3. **Utiliser des satellites comme Starlink**
4. **Utiliser des feux de signalisation**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Solutions possibles

Que font certaines communautés pour fournir un accès Internet dans les zones négligées ?

1. **Elles construisent des tours de télécommunication**
2. **Elles créent leurs propres réseaux communautaires**
3. **Elles installent des antennes sur chaque maison**
4. **Elles se passent d'Internet**

---
![bg cover opacity:.25](https://gitlab.com/adrienK/courscm2/-/raw/main/assets/global_bg.webp)
### Conséquences

Quelle est une conséquence de l'absence d'accès à Internet ?

1. **Facilité à trouver un emploi**
2. **Isolement social et culturel**
3. **Accès facile à l'information**
4. **Utilisation accrue des services publics**
